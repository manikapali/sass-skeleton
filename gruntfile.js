module.exports = function(grunt) {

    grunt.initConfig({
        //merging vendors js file into single plugins.js file
        concat: {
            options: {
                seperator: ';',
            },
            // concatinating or combining different jquery plugins into single plugins.js
            dist: {
                src: [
                    'vendor/modernizr/modernizr.js',
                    'vendor/foundation/js/foundation.min.js',
                    'vendor/foundation/js/foundation/foundation.offcanvas.js',
                    'vendor/slick-carousel/slick/slick.min.js'
                ],
                dest: 'js/plugins.js',
            },
            // creating separate jquery.min.js file in js/ folder
            jquery:{
            	src: 'vendor/jquery/dist/jquery.min.js',
            	dest: 'js/jquery.min.js'
            }
        },
        sass: {
            dist: {
                options: {
                    sourcemap: 'auto', // auto, file, inline, none
                    compass: true, // compass is support to use its functions and mixins
                    loadPath: [
                    			"vendor/foundation/scss/", // foundation scss root folder autoloaded incase needs to be used
                    			"vendor/slick-carousel/slick/", // slick carousel scss root folder autoloaded incase needs to be used
                    			'vendor/font-awesome/scss/' // font-awesome scss root folder autoloaded incase needs to be used
                    		],
                    // require: ['bourbon'], // bourbon framework used; use this by installing bourbon; gem install bourbon
                    noCache: false,
                    style: 'expanded' // nested, compact, compressed, expanded
                },
                files: {
                    'css/style.css': 'scss/main.scss'
                }
            }
        },
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass'],

            },

        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['concat', 'sass', 'watch']);
}
